#Storage Manager for Back2Drive
#Anthony Giuntini
import pickle
import os.path
import os
import sys
import Up2Drive.Back2Drive as b2d
from Settings import *

if DEBUG:
    sys.path.append('/Users/Anthony/Documents/Programming/Python')

def pcklstore(data, dtype):
    '''
    Pickles data and puts in a file based on its type.
    dtype:
        backup: stored in 'Saved Backups'
    '''
    if dtype == 'backup':
        #Data is a backup class
        filepath = '%s/%s.btdb' % (SAVEDBCS, data.name)
        if DEBUG: print('pcklstore: Writing to %s'% filepath)
        with open(filepath, 'wb') as file:
            pickle.dump(data, file)
        if DEBUG: print('pklstore: Write Successful')

def pcklread(id, dtype, filepath=None):
    '''
    Reads pickled data, based on type and identifier.
    id: name of backup
    filename: full path to file
    backup: reads from 'Saved Backups'
    '''
    if dtype == 'backup':
        #Data is a backup class, id is the name
        if filepath is None:
            filepath = '%s/%s.btdb' % (SAVEDBCS, id)
        if not os.path.exists(filepath):
            if DEBUG: print('pcklread: %s does not exist' % filepath)
            return None
        if DEBUG: print('pcklread: Reading from %s' % filepath)
        with open(filepath, 'rb') as file:
            backup_c = pickle.load(file)
        if DEBUG: print('pklread: Read Successful')
        return backup_c

def get_saved():
    '''
    Returns a list of all the names of saved backups.
    '''
    saved_l = []
    for item in os.listdir(SAVEDBCS):
        if '.btdb' in item:
            saved_l.append(item.replace('.btdb',''))
    if DEBUG: print('get_saved: %s' % str(saved_l))
    return saved_l

def delete_saved(tobedeleted):
    '''
    Deletes given saved backups. tobedeleted must be a list.
    '''
    #Treat iterables and non-iterables differently
    if type(tobedeleted) is list or type(tobedeleted) is tuple:
        for item in tobedeleted:
            filepath = os.path.join(SAVEDBCS,('%s.btdb' % item))
            if os.path.exists(filepath):
                os.remove(filepath)
                if DEBUG: print('delete_saved: Deleted %s' % filepath)
            else:
                if DEBUG: print('delete_saved: %s does not exist' % filepath)
    else:
        filepath = os.path.join(SAVEDBCS,('%s.btdb' % tobedeleted))
        if os.path.exists(filepath):
            os.remove(filepath)
            if DEBUG: print('delete_saved: Deleted %s' % filepath)
        else:
            if DEBUG: print('delete_saved: %s does not exist' % filepath)
