import tkinter
from tkinter import *
from Up2Drive.Settings import *

class MenuBar(Menu):
    def __init__(self, root, **options):

        if 'about_command' in options.keys():
            about_command = options['about_command']
            del options['about_command'] #Must delete it otherwise initializing will fail
        else:
            about_command = None

        Menu.__init__(self, root, **options)
        # self.filemenu = Menu(self,tearoff=0)
        # self.add_cascade(label="File", menu=self.filemenu)
        # self.editmenu = Menu(self,tearoff=0)
        # self.add_cascade(label="Edit", menu=self.editmenu)
        # self.backupmenu = Menu(self,tearoff=0)
        # self.add_cascade(label="Backup", menu=self.backupmenu)
        # self.helpmenu = Menu(self,tearoff=0)
        # self.add_cascade(label="Help", menu=self.helpmenu)
        self.applicationmenu = Menu(self, name='apple')
        self.add_cascade(menu=self.applicationmenu)
        self.applicationmenu.add_command(label="About Back2Drive", command=about_command)

    def init_start(self, new_command, open_command, manage_command, help_command):
        '''
        Standard menu options for the main menu.
        '''
        self.delete(1,index2=LAST)
        self.filemenu = Menu(self,tearoff=0)
        self.add_cascade(label="File", menu=self.filemenu)
        self.helpmenu = Menu(self,tearoff=0)
        self.add_cascade(label="Help", menu=self.helpmenu)

        self.filemenu.add_command(label='New Backup', command=new_command)
        self.filemenu.add_command(label='Open Backup', command=open_command)
        self.filemenu.add_separator()
        self.filemenu.add_command(label='Manage Backups', command=manage_command)
        self.helpmenu.add_command(label='Back2Drive Help', command=help_command)

    def init_main(self, new_command, save_command, help_command, start_command,\
    login_command, logout_command):
        '''
        Standard menu options for the main window.
        '''
        self.delete(1,index2=LAST)
        self.filemenu = Menu(self,tearoff=0)
        self.add_cascade(label="File", menu=self.filemenu)
        self.backupmenu = Menu(self,tearoff=0)
        self.add_cascade(label="Backup", menu=self.backupmenu)
        self.helpmenu = Menu(self,tearoff=0)
        self.add_cascade(label="Help", menu=self.helpmenu)


        self.filemenu.add_command(label='New Backup', command=new_command)
        self.filemenu.add_command(label='Save Backup', command=save_command)
        self.helpmenu.add_command(label='Back2Drive Help', command=help_command)
        self.backupmenu.add_command(label='Login', command=login_command)
        self.backupmenu.add_command(label='Logout', command=logout_command)
        self.backupmenu.add_separator()
        self.backupmenu.add_command(label='Start Backup', command=start_command)

class CustomButton(Label):
    def __init__(self, master, *args, **options):
        '''
        A class that makes a button out of a label.
        '''
        #Label.__init__(self)
        okeys = options.keys()
        if 'command' in okeys:
            self.command = options['command']
            del options['command']
        #Default values
        if 'highlightthickness' not in okeys:
            options['highlightthickness'] = '2px'
        if 'highlightbackground' not in okeys:
            options['highlightbackground'] = BCCOLOR

        Label.__init__(self, master, *args, **options)
        self.bind('<Button-1>', self.downclick1)
        self.bind('<ButtonRelease-1>', self.upclick1)

    def downclick1(self, event):
        self.config(bg=BCCOLOR)
        self.command()

    def upclick1(self, event):
        self.config(bg=BNCOLOR)

class CustomCheckbutton(Checkbutton):
    def __init__(self, master, *args, **options):
        '''
        Custom checkbutton that splits the button and label.
        '''
        text = ''
        bg = BGCOLOR
        fg=TCOLOR
        okeys = options.keys()

        if 'text' in okeys:
            text = options['text']
            del options['text']
        if 'bg' in okeys:
            bg = options['bg']
        else:
            options['bg'] = bg
        if 'fg' in okeys:
            fg = options['fg']
        else:
            options['fg'] = fg

        self.base_frame = Frame(master, bg=bg)
        Checkbutton.__init__(self, self.base_frame, *args, **options)
        self.label = Label(self.base_frame,text=text,bg=bg, fg=fg)
        super().grid(column=0, row=0)
        self.label.grid(column=1, row=0)


    def grid(self, **options):
        self.base_frame.grid(options)

class CustomRadiobutton(Radiobutton):
    def __init__(self, master, *args, **options):
        '''
        Custom checkbutton that splits the button and label.
        '''
        text = ''
        bg = BGCOLOR
        fg=TCOLOR
        okeys = options.keys()

        if 'text' in okeys:
            text = options['text']
            del options['text']
        if 'bg' in okeys:
            bg = options['bg']
        else:
            options['bg'] = bg
        if 'fg' in okeys:
            fg = options['fg']
        else:
            options['fg'] = fg

        self.base_frame = Frame(master, bg=bg)
        Radiobutton.__init__(self, self.base_frame, *args, **options)
        self.label = Label(self.base_frame,text=text,bg=bg, fg=fg)
        super().grid(column=0, row=0)
        self.label.grid(column=1, row=0)

    def grid(self, **options):
        self.base_frame.grid(options)
