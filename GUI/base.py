#Base GUI file for Back2Drive
#Anthony Giuntini

import tkinter
import traceback
import tkinter.filedialog as tfd
import tkinter.messagebox as tmb
import httplib2

from tkinter import ttk
from tkinter import *
from Settings import *

#Fix for package
import sys
if DEBUG:
    sys.path.append('/Users/Anthony/Documents/Programming/Python')

import Up2Drive.Back2Drive as B2D
import Up2Drive.storage.manager as sm
import Up2Drive.GUI.Customwidgets as cw

class Help:
    def __init__(self, master, window=None):
        '''
        Displays a help dialog. If no "window" option is given, it will display the about
        menu.
        '''

        self.top = Toplevel(master, bg=BGCOLOR)
        self.top.wm_title('Help')

        self.base_frame = Frame(self.top, bg=BGCOLOR)
        self.base_frame.grid()
        if window == 'startmenu':
            name = 'Start Menu'
            bodytext = 'Click "New Backup" to create empty backup.\n\n'+\
            'To open a previous backup, select it from the "Saved Backup" list and click "Open."\n'+\
            'If your backup is not in the list, choose "From File...", click open, and '+\
            'find your backup file.'
        elif window == 'backupwindow':
            name = 'Backup Window'
            bodytext = 'Add the files or folders that you want to back up in the "Includes"'+\
            ' field using the "+". To remove a file from the list, select the file and '+\
             'click on the "-". Remove all of the files in the list using the "Remove All" button. '+\
             'If you do not want to backup a particular folder or file that is within a folder you '+\
             'are backing up, add the one you wish to exclude to the "Excludes" field.\n\n'+\
             'Options:\n    *Follow links - If you want to include files that are connected to the backup '+\
             'via links.\n    *Walk Directories - If you want the program to include all subfolders of '+\
             'the one you selected. \nStart the backup using the "Start Backup" button.\n'+\
             'If the status button says "Not Logged In," Click on it to log in.\n\n'+\
             'To change the backup name, double click on the title. To save the name, click '+\
             'outside the title.'
        else:
            self.top.wm_title('About')
            name = 'Back2Drive'
            bodytext = 'Back2Drive is a program that allows you to easily upload files to '+\
            'Google Drive, without the need to move them.'

        t_label = Label(self.base_frame, text=name,font=(FONT, 18),\
        justify='center', width=18, bg=BGCOLOR, fg=TCOLOR)
        t_label.grid(row=1, column=2)

        v_label = Label(self.base_frame, text=('Version: %s' % VERSION),font=(FONT, 14),\
        justify='center', width=18, bg=BGCOLOR, fg=TCOLOR)
        v_label.grid(row=2, column=2)

        b_message = Message(self.base_frame, text=(bodytext),font=(FONT, 14),\
        bg=BGCOLOR, fg=TCOLOR, width=360)
        b_message.grid(row=4, column=2)

        c_label = Label(self.base_frame, text='Copyright 2016 Anthony Giuntini',font=(FONT, 14),\
        justify='center', width=36, bg=BGCOLOR, fg=TCOLOR)
        c_label.grid(row=6, column=2)

        Label(self.base_frame,width=5,height=1, bg=BGCOLOR).grid(row=0,column=0)
        Label(self.base_frame,width=5,height=1, bg=BGCOLOR).grid(row=0,column=4)
        Label(self.base_frame,width=5,height=1, bg=BGCOLOR).grid(row=3,column=0)
        Label(self.base_frame,width=5,height=1, bg=BGCOLOR).grid(row=5,column=0)
        Label(self.base_frame,width=5,height=1, bg=BGCOLOR).grid(row=7,column=0)

class MainMenu(Frame):
    def __init__(self, master, menubar):
        '''
        The start page for the program.
        Asks user to create a new or select a saved backup.
        '''
        Frame.__init__(self,master)
        self.master = master
        self.menubar = menubar

        master.wm_title('Back2Drive')
        menubar.init_start(self.launch_home, self.open, lambda: ModifyBackupsDialog(self.master), \
        lambda: Help(self.master, window='startmenu'))

        self.master.config(bg=BGCOLOR)
        self.config(bg=BGCOLOR)
        logo = PhotoImage(file=os.path.join(BASEDIR,'Resources/logo_formatted.gif'))
        w_label = Label(self, image=logo, bg=BGCOLOR)
        w_label.logo = logo
        w_label.grid(row=1, column=1, columnspan=3)
        c_label = Label(self, text='Create a new backup or open a saved backup.', \
        fg=TCOLOR, bg=BGCOLOR)
        c_label.grid(row=2, column=1, columnspan=3)

        new_button = cw.CustomButton(self, text='New Backup', command=self.launch_home)
        new_button.grid(row=4,column=1, columnspan=3)
        new_button.update()

        self.boptions_var = StringVar()
        self.boptions_var.set('Saved Backup')
        options = sm.get_saved()
        options.append('From file...')
        backup_optionmenu = OptionMenu(self, self.boptions_var, *options)
        backup_optionmenu.config(width=15, bg=BGCOLOR)
        backup_optionmenu.grid(row=6,column=1,columnspan=2)
        open_button = cw.CustomButton(self, text='Open', width=9, command=self.open)
        open_button.grid(row=6,column=3)


        #padding labels
        Label(self,width=5,height=1, bg=BGCOLOR).grid(row=0,column=0)
        Label(self,width=5,height=1, bg=BGCOLOR).grid(row=0,column=4)
        Label(self,width=5,height=1, bg=BGCOLOR).grid(row=3,column=0)
        Label(self,width=5,height=1, bg=BGCOLOR).grid(row=5,column=0)
        Label(self,width=5,height=1, bg=BGCOLOR).grid(row=7,column=0)

    def launch_home(self):
        self.destroy()
        HomeWindow(self.master, B2D.Backup(), B2D.GDrive(), self.menubar).grid()

    def open(self):
        selected = self.boptions_var.get()
        if selected == 'From file...':
            filename = tfd.askopenfilename(parent=self.master, title='Select backup file to open', \
            filetypes=[('Back2Drive backup files', '.btdb')])
            if DEBUG: print('MainMenu.open: User chose %s' % filename)
            backup_c = sm.pcklread(selected,'backup',filepath=filename)
            if backup_c is not None:
                HomeWindow(self.master, backup_c, B2D.GDrive(), self.menubar).grid()
                self.destroy()
            else:
                tmb.showerror('Backup Not Found', 'The backup you selected does not exist.')
        elif selected == 'Saved Backup':
            if DEBUG: print('MainMenu.open: No option selected.')
            backup_c = None
            tmb.showinfo('No Option Selected', 'Please select a backup to open.')
        else:
            backup_c = sm.pcklread(selected,'backup')
            #Makesure backup_c is something
            if backup_c is not None:
                HomeWindow(self.master, backup_c, B2D.GDrive(), self.menubar).grid()
                self.destroy()
            else:
                tmb.showerror('Backup Not Found', 'The backup you selected does not exist.')


class HomeWindow(Frame):
    def __init__(self, master, backup_class, service_class, menubar):
        '''
        Main window for the program.
        Shows files in includes, excludes, service, and options
        '''
        Frame.__init__(self, master)
        self.master = master
        self.bc = backup_class
        self.sc = service_class
        self.menubar = menubar

        self.config(bg=BGCOLOR)
        master.wm_title('Back2Drive')
        self.master.config(bg=BGCOLOR)

        self.menubar.init_main(self.main_menu, self.save, \
        lambda: Help(self.master, window='backupwindow'), self.start, \
        self.login, self.logout)

        self.name_var = StringVar()
        self.name_var.set(self.bc.name)

        self.n_label = Label(self, textvariable=self.name_var,font=(FONT, 24),\
        justify='center', width=18, bg=BGCOLOR, fg=TCOLOR)
        self.n_label.grid(row=1, column=3, columnspan=6)
        self.n_label.bind('<Double-Button-1>',self.edit_name)
        #TODO: Change when multiple services
        s_label = Label(self, text='Google Drive Backup',font=(FONT, 14),\
         bg=BGCOLOR, fg=TCOLOR)
        s_label.grid(row=2, column=3, columnspan=6)

        #Set login status
        self.authorized = self.authorize()
        if self.authorized:
            a_status = 'Logged in'
        else:
            a_status = 'Not logged in'
        #loginstatus_button = Button(self, text=a_status, command=self.get_login,\
        #bg=BGCOLOR)
        self.loginstatus_button = cw.CustomButton(self, text=a_status, command=self.login)
        self.loginstatus_button.grid(row=1,column=10, columnspan=2)

        #Frames for containing smaller elements
        options_frame = Frame(self, width=18, bg=BGCOLOR)
        options_frame.grid(row=5,column=1,columnspan=2, sticky='N')
        type_frame = Frame(self, width=18, bg=BGCOLOR)
        type_frame.grid(row=5,column=4,columnspan=2, sticky='N')
        includes_frame = Frame(self, width=23, bg=BGCOLOR)
        includes_frame.grid(row=5,column=7,columnspan=2, sticky='N')
        excludes_frame = Frame(self, width=23, bg=BGCOLOR)
        excludes_frame.grid(row=5,column=10,columnspan=2, sticky='N')

        #Options Frame
        o_label = Label(options_frame, text='Options',font=(FONT, 18), bg=BGCOLOR, fg=TCOLOR)
        o_label.grid(row=0,column=0)
        # self.f_links_var = IntVar()
        # self.f_links_var.set(self.bc.options['follow_links'])
        # fl_cb = cw.CustomCheckbutton(options_frame, text="Follow Links", variable=self.f_links_var,\
        # bg=BGCOLOR, fg=TCOLOR)
        # fl_cb.grid(row=1,column=0, sticky='W')
        # fl_cb.bind('<Button-1>',lambda event: self.change_option('follow_links', self.f_links_var))
        self.w_dirs_var = IntVar()
        self.w_dirs_var.set(self.bc.options['walk_dirs'])
        wd_cb = cw.CustomCheckbutton(options_frame, text="Walk Directories", variable=self.w_dirs_var, \
        bg=BGCOLOR, fg=TCOLOR)
        wd_cb.grid(row=2,column=0, sticky='W')
        wd_cb.bind('<Button-1>',lambda event: self.change_option('walk_dirs', self.w_dirs_var))

        #Type Frame
        #TODO: Change when more types available
        t_label = Label(type_frame, text='Type',font=(FONT, 18), bg=BGCOLOR, fg=TCOLOR)
        t_label.grid(row=0,column=0)
        self.type_var = StringVar()
        self.type_var.set('clone')
        clone_rb = cw.CustomRadiobutton(type_frame, text="Clone", variable=self.type_var, value='clone', \
        bg=BGCOLOR, foreground=TCOLOR)
        clone_rb.grid(row=1,column=0, sticky='W')

        #Includes Frame
        i_label = Label(includes_frame, text='Included',font=(FONT, 18), bg=BGCOLOR,\
        fg=TCOLOR)
        i_label.grid(row=0,column=0)
        self.includes_lb = Listbox(includes_frame, width=23, selectmode=EXTENDED, \
        bg=BGCOLOR, fg=TCOLOR, selectbackground=BCCOLOR)
        for ipath in self.bc.includes:
            self.includes_lb.insert(END, ipath)
        self.includes_lb.grid(column=0)
        #Action buttons (bound to labels) for listbox
        iact_frame = Frame(includes_frame, bg=BGCOLOR)
        iact_frame.grid(column=0)
        ir_label = cw.CustomButton(iact_frame, text='-',font=(FONT, 14), width=6, justify='center',\
        command=self.i_remove)
        ir_label.grid(row=0,column=0)
        ira_label = cw.CustomButton(iact_frame, text='Remove All',font=(FONT, 14), \
        width=11, justify='center', command=self.i_remove_all)
        ira_label.grid(row=0,column=1)
        ia_label = cw.CustomButton(iact_frame, text='+',font=(FONT, 14), width=6, \
        justify='center', command=self.i_add)
        ia_label.grid(row=0,column=2)

        #Excludes Frame
        e_label = Label(excludes_frame, text='Excluded',font=(FONT, 18), bg=BGCOLOR,\
        fg=TCOLOR)
        e_label.grid(row=0,column=0)
        self.excludes_lb = Listbox(excludes_frame, width=23, selectmode=EXTENDED, \
        bg=BGCOLOR, fg=TCOLOR, selectbackground=BCCOLOR)
        for epath in self.bc.excludes:
            self.excludes_lb.insert(END, epath)
        self.excludes_lb.grid(column=0)
        #Action buttons (bound to labels) for listbox
        eact_frame = Frame(excludes_frame)
        eact_frame.grid(column=0)
        er_label = cw.CustomButton(eact_frame, text='-',font=(FONT, 14), width=6, \
        justify='center', command=self.e_remove)
        er_label.grid(row=0,column=0)
        era_label = cw.CustomButton(eact_frame, text='Remove All',font=(FONT, 14), width=11,\
        justify='center', command=self.e_remove_all)
        era_label.grid(row=0,column=1)
        ea_label = cw.CustomButton(eact_frame, text='+',font=(FONT, 14), width=6, \
        justify='center', command=self.e_add)
        ea_label.grid(row=0,column=2)

        start_button = cw.CustomButton(self, text="Start Backup", command=self.start,\
        width=9)
        start_button.grid(row=7,column=5, columnspan=3)
        menu_button = cw.CustomButton(self, text="Menu", command=self.main_menu,\
        width=9)
        menu_button.grid(row=7,column=7,columnspan=3)
        help_button = cw.CustomButton(self, text="?", command=lambda: Help(self.master, window='backupwindow'))
        help_button.grid(row=7,column=10, columnspan=2, sticky='E')

        #Padding labels
        Label(self,width=5,height=1, bg=BGCOLOR).grid(row=0,column=0)
        Label(self,width=5,height=1, bg=BGCOLOR).grid(row=4,column=9)
        Label(self,width=5,height=1, bg=BGCOLOR).grid(row=0,column=15)
        Label(self,width=5,height=1, bg=BGCOLOR).grid(row=3,column=0)
        Label(self,width=5,height=1, bg=BGCOLOR).grid(row=5,column=0)
        Label(self,width=5,height=1, bg=BGCOLOR).grid(row=6,column=0)
        Label(self,width=5,height=1, bg=BGCOLOR).grid(row=5,column=6)
        Label(self,width=5,height=1, bg=BGCOLOR).grid(row=5,column=3)
        Label(self,width=5,height=1, bg=BGCOLOR).grid(row=8,column=0)

    def main_menu(self):
        self.destroy()
        MainMenu(self.master, self.menubar).grid()

    def destroy(self):
        '''
        Called when the user destroys the frame.
        '''
        if self.bc_has_changed():
            if DEBUG: print('HomeWindow.exit: Backup class has changed')
            #If the user responds positivly, save the backup
            if tmb.askyesno('Save Backup?', 'Do you wish to save this backup?'):
                self.save()
                #FIXME destroyed before name change
        Frame.destroy(self)

    def save(self):
        '''
        Saves the backup class instance.
        '''
        if self.bc.name in sm.get_saved():
            if tmb.askyesno('Backup Already Exists', 'Do you wish to overwrite the previous backup?', default='no'):
                sm.pcklstore(self.bc, 'backup')
            else:
                self.new_name()
                sm.pcklstore(self.bc, 'backup')
        elif self.bc.name == 'New Backup':
            tmb.showinfo('Invalid Name', 'Please enter a different name for your backup.')
            self.new_name()
            sm.pcklstore(self.bc, 'backup')
        else:
            sm.pcklstore(self.bc, 'backup')

    def new_name(self):
        '''
        Allows the user to input a new name for the backup.
        '''
        gdinput = GenericDialog(self.master, 'input', message='Enter a new name:',\
        choice1='Cancel', choice2='Save', title='New Name')
        if gdinput != 'Cancel':
            new_name = gdinput.get_response()
            if DEBUG: print('HomeWindow.save: User inputed: %s' % new_name)
            if new_name is not None and new_name != '':
                self.bc.name = new_name
                self.name_var.set(new_name)
                if DEBUG: print('HomeWindow.save: Name changed to %s' % self.bc.name)
                sm.pcklstore(self.bc, 'backup')
            elif new_name == '':
                tmb.showinfo('Invalid Name', 'Please enter a valid name.')
                self.new_name()


    def edit_name(self, event, close=False):
        '''
        Makes the title label an entry widget and allows the user
        to change the name.
        '''
        if not close:
            self.n_label.grid_remove()
            self.name_e = Entry(self, textvariable=self.name_var, width=18,\
            bg=BGCOLOR, font=(FONT, 20), justify='center',highlightbackground=BCCOLOR,\
            fg=TCOLOR)
            self.name_e.grid(row=1, column=3, columnspan=6)
            self.name_e.focus_set()
            self.bind('<Button-1>', lambda event: self.edit_name(event, close=True))
        else:
            self.name_e.grid_remove()
            self.n_label.grid(row=1, column=3, columnspan=6)
            self.bc.name = self.name_var.get()
            self.unbind('<Button-1>')

    def i_add(self):
        gd = GenericDialog(self.master, 'choice', message='Do you want to select files or folders?',\
        choice1='Files', choice2='Folders', title='Files or Folders?')
        f_or_d = gd.get_response()
        if DEBUG: print('HomeWindow.i_add: User selected: %s' %f_or_d)
        if f_or_d == 'Folders':
            selected = [tfd.askdirectory(parent=self.master, title='Select directories to exclude')]
        elif f_or_d == 'Files':
            selected = tfd.askopenfilename(parent=self.master, multiple=True, title='Select files to exclude')
        else:
            selected = []
        if len(selected) != 0 and selected[0] is not None:
            for item in selected:
                if item != '':
                    self.bc.includes.append(item)
                    self.includes_lb.insert(END, item)
                    self.includes_lb.update()
                if DEBUG:
                    print('HomeWindow.i_add: Added %s to includes.'% item)

    def i_remove(self):
        selected = self.includes_lb.curselection()
        if len(selected) != 0:
            for index in selected:
                item = self.includes_lb.get(index)
                try:
                    self.bc.includes.remove(item)
                    self.includes_lb.delete(index)
                    self.includes_lb.update()
                    if DEBUG: print('HomeWindow.i_remove: Removed %s from includes' % item)
                except ValueError:
                    if DEBUG:
                        print('HomeWindow.i_remove:')
                        traceback.print_exc()

    def i_remove_all(self):
        self.bc.includes.clear()
        self.includes_lb.delete(0, END)
        self.includes_lb.update()
        if DEBUG: print('HomeWindow.i_remove_all: Cleared includes.')

    def e_add(self):
        gd = GenericDialog(self.master, 'choice', message='Do you want to select files or folders?',\
         choice1='Files', choice2='Folders', title='Files or Folders?')
        f_or_d = gd.get_response()
        if DEBUG: print('HomeWindow.e_add: User selected: %s' %f_or_d)
        if f_or_d == 'Folders':
            selected = [tfd.askdirectory(parent=self.master, title='Select directories to exclude')]
        elif f_or_d == 'Files':
            selected = tfd.askopenfilename(parent=self.master, multiple=True, title='Select files to exclude')
        else:
            selected = []
        if len(selected) != 0 and selected[0] is not None:
            for item in selected:
                if item != '':
                    self.bc.excludes.append(item)
                    self.excludes_lb.insert(END, item)
                    self.excludes_lb.update()
                if DEBUG:
                    print('HomeWindow.e_add: Added %s to excludes.'% item)

    def e_remove(self):
        selected = self.excludes_lb.curselection()
        if len(selected) != 0:
            for index in selected:
                item = self.excludes_lb.get(index)
                try:
                    self.bc.excludes.remove(item)
                    self.excludes_lb.delete(index)
                    self.excludes_lb.update()
                    if DEBUG: print('HomeWindow.e_remove: Removed %s from excludes' % item)
                except ValueError:
                    if DEBUG:
                        print('HomeWindow.e_remove:')
                        traceback.print_exc()

    def e_remove_all(self):
        self.bc.excludes.clear()
        self.excludes_lb.delete(0, END)
        self.excludes_lb.update()
        if DEBUG: print('HomeWindow.e_remove_all: Cleared excludes.')

    def start(self):
        if self.authorized:
            if DEBUG: print('HomeWindow.start: starting backup')
            pd = ProgressDialog(self.master)
            self.master.after(100,lambda: self.handoff2main(pd))

        else:
            if DEBUG: print('HomeWindow.start: Not Authorized')
            #TODO: Alert user to login

    def login(self):
        self.authorize()
        self.loginstatus_button.config(text='Logged in')

    def logout(self):
        '''
        Deletes the credentials file.
        '''
        credential_path = os.path.join(USER_AS, 'credentials/creds.json')
        if os.path.exists(credential_path):
            os.remove(credential_path)
            if DEBUG: print('Authorization.remove_credentials: Deleted json')
        else:
            pass
        self.loginstatus_button.config(text='Not Logged in')

    def authorize(self):
        '''
        Authorizes the user for the particular service and returns the status.
        '''
        return self.sc.obtain_authorization()

    def handoff2main(self, pd):
        '''
        Workaround for multiple start commands in after function.
        '''
        if len(self.bc.includes) != 0:
            #Expand excludes
            self.bc.scan_files(pd=pd, for_includes=False)
            #Scan includes
            self.bc.scan_files(pd=pd)
            try:
                self.sc.init_backup(self.bc, pd=pd)
            except httplib2.ServerNotFoundError:
                if DEBUG: print("HomeWindow.handoff2main: Connection Failed")
                tmb.showerror('Connection Failed', 'Back2Drive was unable to connect to the internet.\n'+\
                'Check your internet connection and try again.')
                pd.destroy()
        else:
            if DEBUG: print("HomeWindow.handoff2main: No Files to backup")
            tmb.showerror('No Files Selected', 'Please select the files/folders you want to backup.')
            pd.destroy()


    def bc_has_changed(self):
        '''
        Checks to see if the backup class has changed.
        returns a bool: true if changed.
        '''
        if len(self.bc.includes) == 0 and len(self.bc.excludes) == 0:
            return False
        elif self.bc.name not in sm.get_saved():
            return True
        else:
            saved_version = sm.pcklread(self.bc.name, 'backup')
        #check dirs
        #FIXME: Excludes seen as different due to path expansion
        if saved_version.includes != self.bc.includes or saved_version.excludes != self.bc.excludes:
            return True
        elif saved_version.options != self.bc.options:
            return True
        else:
            return False

    def change_option(self, option, variable):
        '''
        Changes the specified option to whatever the given tkinter variable holds.
        '''
        #Note: 'not' solves problem of checkbox updating after event
        self.bc.options[option] = not variable.get()
        if DEBUG: print('HomeWindow.change_option: Changed option %s to %d.'%(option, not variable.get()))

class ModifyBackupsDialog(Toplevel):
    def __init__(self, master, **options):
        '''
        Allows user to delete saved backups.
        '''

        options['bg'] = BGCOLOR
        super().__init__(master, **options)

        self.wm_title('Saved Backups')

        base_frame = Frame(self, bg=BGCOLOR)
        base_frame.grid(padx=20,pady=20)

        #Includes Frame
        title_l = Label(base_frame, text='Saved Lists',font=(FONT, 18), bg=BGCOLOR,\
        fg=TCOLOR)
        title_l.grid(row=0,column=0)
        self.saved_lb = Listbox(base_frame, width=23, selectmode=EXTENDED, \
        bg=BGCOLOR, fg=TCOLOR, selectbackground=BCCOLOR)
        for name in sm.get_saved():
            self.saved_lb.insert(END, name)
        self.saved_lb.grid(column=0)
        #Action buttons (bound to labels) for listbox
        act_frame = Frame(base_frame, bg=BGCOLOR)
        act_frame.grid(column=0, pady=3)
        r_label = cw.CustomButton(act_frame, text='Remove',font=(FONT, 14), width=11, justify='center',\
        command=self.remove)
        r_label.grid(row=0,column=0)
        ra_label = cw.CustomButton(act_frame, text='Remove All',font=(FONT, 14), \
        width=11, justify='center', command=self.remove_all)
        ra_label.grid(row=0,column=2)

        Label(act_frame, bg=BGCOLOR).grid(row=0,column=1,padx=4)

    def remove(self):
        '''
        Deletes the selected backups.
        '''
        index = self.saved_lb.curselection()
        for index in self.saved_lb.curselection():
            sm.delete_saved(self.saved_lb.get(index))
            self.saved_lb.delete(index)

    def remove_all(self):
        '''
        Deletes all of the saved backups.
        '''
        items = self.saved_lb.get(0,END)
        print(items)
        sm.delete_saved(items)
        # #make sure items is a list
        # if type(items) == list:
        #
        # else:
        #     sm.delete_saved([items)
        self.saved_lb.delete(0,END)


class ProgressDialog(Toplevel):
    def __init__(self, master, **options):
        self.master = master
        options['bg'] = BGCOLOR
        super().__init__(master, options)
        self.wm_title('Backup Progress')

        ttkstyle = ttk.Style()
        ttkstyle.theme_use('alt')
        ttkstyle.configure('TProgressbar',background=BCCOLOR, foreground='red')

        self.description_l = Label(self, text='Backing up...', width=45, \
        justify='left', font=(FONT, 12), bg=BGCOLOR)
        self.description_l.grid(column=1,row=1)

        self.progressbar = ttk.Progressbar(self, length=300, orient='horizontal', \
        mode='determinate', style='TProgressbar')
        self.progressbar.grid(column=1, row=3)

        Label(self,width=5,height=1, bg=BGCOLOR).grid(row=0,column=0)
        Label(self,width=5,height=1, bg=BGCOLOR).grid(row=4,column=2)

    def new_progressbar(self, length=100):
        self.progressbar.destroy()
        self.progressbar = ttk.Progressbar(self, length=300, orient='horizontal', mode='determinate', maximum=length)
        self.progressbar.grid(column=1, row=2)

    def step_pb(self, labelmsg=None, amount=1):
        self.progressbar.step(amount=amount)
        self.progressbar.update()
        if labelmsg is not None:
            self.description_l.config(text=labelmsg)
            self.description_l.update()

    def clear(self):
        self.progressbar.destroy()

    def close(self):
        self.destroy()

class GenericDialog:
    def __init__(self, master, dtype, message='Choose yes/no', choice1='Yes', choice2='No', title=''):
        '''
        Displays a simple dialog. Returns the selected choice
        '''
        self.master = master
        self.dtype = dtype

        self.top = Toplevel(self.master, bg=BGCOLOR)
        self.top.wm_title(title)

        message_label = Label(self.top, text=message, font=(FONT, 14), justify='left',\
        bg=BGCOLOR)
        message_label.grid(row=1, column=1, columnspan=4)

        self.response = StringVar()

        def set_choice(choice):
            self.response.set(choice)
            self.top.destroy()

        if self.dtype == 'choice':
            #simple choice dialog
            button1 = cw.CustomButton(self.top, text=choice1, command=lambda: set_choice(choice1), width=6)
            button2 = cw.CustomButton(self.top, text=choice2, command=lambda: set_choice(choice2), width=6)
            button1.grid(row=3,column=2)
            button2.grid(row=3,column=4)

            Label(self.top, width=6, bg=BGCOLOR).grid(row=2,column=0)
            Label(self.top, width=2, bg=BGCOLOR).grid(row=3,column=3)
            Label(self.top, width=12, bg=BGCOLOR).grid(row=3,column=1)

        elif self.dtype == 'input':
            #Simple entry dialog
            self.name_e = Entry(self.top, textvariable=self.response, width=25,\
            font=(FONT, 14), justify='center', highlightbackground=BCCOLOR,\
            fg=BLACK)
            self.name_e.grid(row=2, column=1, columnspan=4, pady=12)

            button1 = cw.CustomButton(self.top, text=choice1, command=self.cancel, width=6)
            button2 = cw.CustomButton(self.top, text=choice2, command=self.get_response, width=6)
            button1.grid(row=3,column=2)
            button2.grid(row=3,column=4)

            Label(self.top, width=6, bg=BGCOLOR).grid(row=2,column=0)
            Label(self.top, width=2, bg=BGCOLOR).grid(row=3,column=3)
            Label(self.top, width=12, bg=BGCOLOR).grid(row=3,column=1)

        Label(self.top, width=6, bg=BGCOLOR).grid(row=0,column=0)
        Label(self.top, width=6, bg=BGCOLOR).grid(row=4,column=5)

        self.master.wait_window(window=self.top)

    def get_response(self):
        if self.dtype == 'choice':
            return self.response.get()
        if self.dtype == 'input':
            self.top.destroy()
            return self.response.get()
        else: return None

    def cancel(self):
        self.top.destroy()
        return None

def run():
    root = Tk()
    menubar = cw.MenuBar(root, about_command=lambda: Help(root))
    MainMenu(root, menubar).grid()
    root.config(menu=menubar)
    root.mainloop()

if __name__ == "__main__":
    run()
