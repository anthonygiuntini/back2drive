#Settings file for Back2Drive
#Anthony Giuntini

import os
import sys

VERSION = '0.0 dev'

#Set this to false when deploying Application
DEBUG = True

FONT = 'Avenir'

BGCOLOR = '#%02x%02x%02x' % (105,120,126) #Convert rgb to tkinter
BNCOLOR = '#%02x%02x%02x' % (255,255,255)#(69, 123, 154)
BCCOLOR = '#%02x%02x%02x' % (50, 105, 124)
TCOLOR = '#%02x%02x%02x' % (255,255,255)#(112, 184, 226)
T1COLOR = '#%02x%02x%02x' % (211, 175, 130)
BLACK = '#%02x%02x%02x' % (0, 0, 0)
#Note: Modify the path if the file location is changed
FROZEN = hasattr(sys, 'frozen')
if FROZEN:
    BASEDIR = os.path.join(os.path.abspath(__file__)).replace('/Resources/lib/python33.zip/Settings.pyc','')
else:
    BASEDIR = os.path.dirname(os.path.abspath(__file__))

USER_AS = os.path.join(os.path.expanduser('~'),'Library/Application Support/Back2Drive')
SAVEDBCS = os.path.join(USER_AS,'Saved Backups')
