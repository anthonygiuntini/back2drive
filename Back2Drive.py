
#Back2Drive
#Anthony Giuntini

import httplib2
import os
import os.path
import argparse
import traceback
import time
import webbrowser
import http.server
from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools
from apiclient.http import MediaFileUpload
import sys
from Settings import *
if DEBUG:
    sys.path.append('/Users/Anthony/Documents/Programming/Python')

#TODO: figure out workaround
CODE = None
class HTTPHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        global CODE
        self.code = self.path.replace('/?code=','')
        if DEBUG: print('HTTPHandler.do_GET: request: %s' % self.path)
        CODE = self.code
        return self.code

class Authorization:
    def __init__(self):
        self.credentials = None
        self.storage = None
        self.CSF = 'client_secret.json'
        self.SCOPE = 'https://www.googleapis.com/auth/drive.file'
        self.A_NAME = 'Back2Drive'

    def credential_storage(self, credentials=None):
        '''
        Returns api credentials storage object.
        '''
        #TODO: Change storage location
        credential_dir = os.path.join(USER_AS, 'credentials')
        if not os.path.exists(credential_dir):
            os.mkdir(credential_dir)
        credential_path = os.path.join(credential_dir, 'creds.json')
        self.storage = oauth2client.file.Storage(credential_path)
        if credentials is None:
            self.credentials = self.storage.get()
            return self.storage
        else:
            self.storage.put(credentials)
            return True

    def credentials_valid(self):
        self.credential_storage()
        if not self.credentials or self.credentials.invalid:
            if DEBUG: print('Authorization.credentials_valid: Credentials not valid')
            return False
        else:
            return True

    def update_credentials(self):
        global CODE
        if self.credentials_valid():
            pass
        else:
            flow = client.flow_from_clientsecrets(self.CSF, self.SCOPE, 'http://localhost:8000')
            a_url = flow.step1_get_authorize_url()
            handler = HTTPHandler
            webbrowser.open_new(a_url)
            httpd = http.server.HTTPServer(('localhost',8000), handler)
            request = httpd.handle_request()
            if DEBUG: print(CODE)
            self.credentials = flow.step2_exchange(CODE)
            self.credential_storage(credentials=self.credentials)
            del CODE
        return self.credentials

    def authorize(self):
        return self.credentials.authorize(httplib2.Http())

class Backup:
    def __init__(self):
        self.includes = []
        #FIXME: Excludes doesn't work
        #FIXME: follow_links option
        self.excludes = []
        self.options = {'follow_links':False, 'walk_dirs':True}
        self.paths = [] #contains relpath from base_dir
        #TODO: Make sure base dir includes all directories in 'includes'
        self.base_dir = '/Users/Anthony/Documents' #Note: cannot end with a /
        self.name = 'New Backup'
        self.backup_size = 0

    def walk_path(self, path, for_includes=True):
        '''
        Walks specified path and returns os.walk generator.
        Either for includes(True) or excludes.
        '''
        print(self.excludes)
        l_paths = []
        for bpath, dirs, files in os.walk(path, followlinks=self.options['follow_links']):
            cdir = bpath
            for f in files:
                if for_includes:
                    file_relpath = os.path.relpath(os.path.join(cdir, f), start=self.base_dir)
                else:
                    file_relpath = os.path.join(cdir, f)#'%s/%s'%(cdir,f)
                #check if it has already been added
                if file_relpath not in self.paths or not for_includes:
                    #Check if it is in excludes
                    if os.path.join(self.base_dir, file_relpath) not in self.excludes:
                        #If walk_dirs is false, the curent dir path must match the original
                        if self.options['walk_dirs'] or cdir == path or not for_includes:
                            if DEBUG: print('Current relpath: %s' % file_relpath)
                            self.backup_size += os.stat('%s/%s'%(cdir,f)).st_size
                            l_paths.append(file_relpath)
        return l_paths

    def scan_files(self,pd=None, for_includes=True):
        '''
        Takes the paths in the self.includes list, excluding the paths
        in the self.excludes list and adds all of the file paths to a new list.
        If it is a dir, it will walk the dir (optional). It if is a link
        it will follow the link (optional). pd is the progressdialog.
        For includes if True otherwise extends excludes
        '''
        self.paths = []
        self.base_dir = self.get_base_dir()
        if for_includes:
            c_list= self.includes
        else:
            c_list = self.excludes
        l_paths = []
        if pd is not None:
            pd.new_progressbar(length=len(c_list))
        for obj in c_list:
            file_relpath = os.path.relpath(obj,start=self.base_dir)
            if DEBUG: print('Backup.scan_files: scanning %s' % obj)
            if pd is not None:
                pd.step_pb(labelmsg=('Scanning %s' % obj))
            #Walk if a dir and if dirs are to be walked
            if os.path.isdir(obj):# and self.options['walk_dirs']:
                l_paths.extend(self.walk_path(obj, for_includes=for_includes))
            #if it is a file not already in self.paths add it
            elif os.path.isfile(obj) and file_relpath not in l_paths:
                if obj not in self.excludes:
                    l_paths.append(file_relpath)
            else:
                if DEBUG: print(('Backup.scan_files: %s is not a file, link, or directory.' % obj))
        if for_includes:
            self.paths.extend(l_paths)
        else:
            self.excludes.extend(l_paths)
        return l_paths

    def get_base_dir(self):
        '''
        Find the base path to use as a reference point for later path operations.
        '''

        paths_dir = self.includes
        #Make sure there is a path to start with.
        try:
            current = paths_dir[0]
        except:
            if DEBUG: print('Backup.get_base_dir: No files selected')
            current = '/'
        current = paths_dir[0]
        unfound = True
        previous = ''
        while unfound:
            #Find common path
            for i, path in enumerate(paths_dir):
                cdirp, name = os.path.split(path)
                if current.startswith(cdirp):
                    current = cdirp
            count = 0
            #Check all paths in includes
            for path in self.includes:
                if path.startswith(current):
                    count+=1
            if len(self.includes) == count:
                unfound = False
                base_p = current
            else:
                paths_dir = [os.path.split(p)[0] for p in paths_dir]

        if DEBUG: print('Backup.get_base_dir: Base Dir: %s' % base_p)

        return base_p


class GDrive:
    def __init__(self):
        '''
        Class that handles uploading the files to Google Drive.
        To complete the upload call in order:
        1) init_backup(backup_class)
        2) prepare_backup()
        3) upload_backup() (only if you are not in debug mode)
        '''
        self.auth = None

    def obtain_authorization(self):
        #TODO: If not first time remobe update_credentials
        try:
            auth_c = Authorization()
            auth_c.update_credentials()
            self.auth = auth_c.authorize()
            return True
        except:
            if DEBUG: traceback.print_exc()
            return False

    def init_backup(self, backup, pd=None):
        '''
        Authorizes, creates new service, and starts the backup.
        pd is the progressdialog.
        '''
        if DEBUG: print('GDrive.init_backup: Initializing backup')
        self.pd = pd
        self.backup = backup
        if self.pd is not None:
            self.pd.new_progressbar(length=(len(self.backup.paths)+1))

        self.dir_id_dict = {}
        self.prepped_files = []
        self.basedirname = os.path.basename(self.backup.base_dir)
        if self.pd is not None:
            self.pd.step_pb(labelmsg='Authorizing')
        self.obtain_authorization()
        self.service = self.new_service()
        self.prepare_backup()
        if self.pd is not None:
            self.pd.close()
            pass
        if DEBUG: print('Backup complete. \n# files in backup: %d' % len(self.backup.paths))

    def new_service(self):
        '''
        Creates new google drive service.
        '''
        return discovery.build('drive', 'v3', http=self.auth)

    def change_backup(self, backup):
        self.backup = backup

    def create_directory(self, name, parent):
        '''
        Creates a google Drive directory.
        '''
        if self.pd is not None:
            self.pd.step_pb(labelmsg='Creating directory "%s" in backup location' % name)
        if parent != None:
            try:
                parentid = self.dir_id_dict[parent]
            except KeyError:
                parentid = self.dir_id_dict[self.backup.name]
            metadata = {
            'name': name,
            'parents': [parentid],
            'mimeType': 'application/vnd.google-apps.folder'
            }
        else:
            metadata = {
            'name': name,
            'mimeType': 'application/vnd.google-apps.folder'
            }
        directory = self.service.files().create(body=metadata, fields='id').execute()
        dir_id = directory.get('id')

        self.dir_id_dict[name] = dir_id
        if DEBUG: print('GDrive.create_directory: Created Directory %s with id %s' % (name,dir_id))
        return dir_id

    def prepare_backup(self):
        '''
        Creates google drive folders for the backup.
        '''
        if DEBUG: print('Preparing backup...\nBackup size: '+str(round(self.backup.backup_size/1048576,4))+'MB')

        self.create_directory(self.backup.name,None)
        index = 0
        for path in self.backup.paths:
            #Workaround for a bug that skips the base dir of the path
            # if path.split('/')[0] not in self.dir_id_dict.keys():
            #     self.create_directory(path.split('/')[0], self.backup.name)
            #Prepare files in chunks of 100. Upload them after 100 is reached.
            if index < 100:
                self.check_dirs(path)
                self.create_media(path)
                index += 1
            else:
                if DEBUG: print('GDrive.prepare_backup: Uploading %d files' % len(self.prepped_files))
                self.upload_backup()
                del self.prepped_files[:]
                index = 0
        if len(self.prepped_files) != 0:
            if DEBUG: print('GDrive.prepare_backup: Uploading %d files' % len(self.prepped_files))
            self.upload_backup()


    def create_media(self,path):
        '''
        Add path (if a file) to the list of media objects that are ready to be uploaded.
        '''
        abs_path = os.path.join(self.backup.base_dir, path)
        if os.path.isfile(abs_path):
            if self.pd is not None:
                self.pd.step_pb(labelmsg=('Preparing: %s' % path))
            if DEBUG: print('GDrive.create_upload: Preparing %s' % abs_path)
            media = MediaFileUpload(abs_path, resumable=True, mimetype='application/octet-stream')
            parent = os.path.basename(os.path.dirname(path))
            if parent == '':
                parent = self.backup.name
            metadata = {
            'name': os.path.basename(abs_path),
            'parents':[self.dir_id_dict[parent]],
            'description': 'Backup file',
            }
            self.prepped_files.append((media,metadata))

    def check_dirs(self, path):
        '''
        Breaks down a path and creates a drive folder if one doesnt exist.
        '''
        dirs_l = []
        dir_p = os.path.dirname(path)
        #Split the path into a list of names.
        while os.path.split(dir_p)[1] != '':
            spl = os.path.split(dir_p)
            dirs_l.append(spl[1])
            dir_p = spl[0]
        dirs_l.reverse() #correction for path being split backwards

        if DEBUG: print('GDrive.check_dirs: path split: %s' % str(dirs_l))
        for i in range(len(dirs_l)):
            if dirs_l[i] not in self.dir_id_dict.keys():
                if i != 0:
                    parentn = dirs_l[i-1]
                else:
                    parentn = self.basedirname
                try:
                    self.create_directory(dirs_l[i], parentn)
                except:
                    if DEBUG:
                        print('GDrive.check_dirs: ')
                        traceback.print_exc()
                    attempts = 1
                    status = False
                    while attempts <=5 and status is not True:
                        #throttle the speed to let server catch up
                        time.sleep(.15*attempts)
                        try:
                            self.create_directory(dirs_l[i], parentn)
                            status = True
                        except:
                            if DEBUG:
                                print('GDrive.check_dirs: attempt #%d'%attempts)
                                traceback.print_exc()
                            attempts += 1

    def upload_backup(self):
        '''
        Uploads the backup (from the prepped_files) to Drive.
        '''
        index = 0
        self.failed = []
        for media in self.prepped_files:
            if DEBUG: print('GDrive.upload_backup: Starting upload for %s' % media[1]['name'])
            if self.pd is not None:
                self.pd.step_pb(labelmsg=('Uploading %s' % media[1]['name']))
            request = self.service.files().create(body=media[1], media_body=media[0])
            response = None
            status = None
            pstatus = 0
            attempts = 0
            while response is None:
                try:
                    status, response = request.next_chunk()
                    if self.pd is not None and status is not None:
                        self.pd.step_pb(labelmsg=('Uploading %s' % media[1]['name']),amount=((status.progress()-pstatus)))
                        pstatus = status.progress()
                except:
                    if DEBUG:
                        print('GDrive.upload_backup: attempt #%d' % attempts)
                        traceback.print_exc()
                    if attempts >= 5:
                        self.failed.append(media[1]['name'])
                        response = False
                        if DEBUG: print('GDrive.upload_backup: Gave up on upload for: %s'% media[1]['name'])
                    attempts += 1
                if DEBUG and status is not None: print('GDrive.upload_backup: status = %d' % (status.progress()*100))
            if DEBUG: print('GDrive.upload_backup: Finished upload')
        if DEBUG: print('GDrive.upload_backup: Files uploaded')

if __name__ == '__main__':
    if DEBUG:
        print(('='*30))
        print(('Back2Drive Version: %s' % version))
        print('Copyright 2016 Anthony Giuntini \nDEBUG MODE')
        print(('='*30))
    b = Backup()
    b.includes.append('/Users/Anthony/Desktop/test')
    gd = GDrive()
    gd.init_backup(b)
