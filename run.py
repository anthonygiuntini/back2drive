#Back2Drive
#Anthony Giuntini

import GUI.base as gbase
import Back2Drive as b2d
import traceback
import sys
import time
import os
import os.path
from tkinter import Tk
import tkinter.messagebox as tmb
from Settings import *

sys.path.append('/Users/Anthony/Documents/Programming/Python')

if __name__ == '__main__':
    #Catch error
    try:
        #If first time
        if not os.path.exists(USER_AS):
            print('run: First time. Creating Application Support dir')
            os.mkdir(USER_AS)
            os.mkdir(SAVEDBCS)

        gbase.run()
    except SystemExit:
        #Catch command-Q
        if DEBUG: print('run: SYSTEM EXIT')

    except:
        if DEBUG:
            traceback.print_exc()
        else:
            with open(os.path.join(USER_AS,'ErrorLog.txt'),'a') as file:
                y,mn,d,h,m,s,a,b,c = time.localtime()
                file.write("==================="+str(mn)+'/'+str(d)+' '+
                           str(h)+':'+str(m)+':'+str(s)+
                           "=====================\n")
                traceback.print_exc(file=file)
            root = Tk()
            root.withdraw()
            tmb.showerror('Fatal Error', 'Sorry:( Back2Drive was killed by a bug.\n'+\
            'If this problem persists, please send us a note.')
