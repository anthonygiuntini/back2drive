"""
This is a setup.py script generated by py2applet

Usage:
    python setup.py py2app
"""

from setuptools import setup
import sys
sys.path.append('/Users/Anthony/Documents/Programming/Python')

APP = ['run.py']
DATA_FILES = ['Resources/logo_formatted.gif','/Users/Anthony/Documents/Programming/Python/Up2Drive/lib/python3.3/site-packages/httplib2/cacerts.txt']
OPTIONS = {'argv_emulation': False, 'includes':[], 'iconfile':'icon.icns',
'excludes':[]}

setup(
    app=APP,
    data_files=DATA_FILES,
    options={'py2app': OPTIONS},
    setup_requires=['py2app'],
)
